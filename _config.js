import lume from "lume/mod.ts";
import attributes from "lume/plugins/attributes.ts";
import code_highlight from "lume/plugins/code_highlight.ts";
import date from "lume/plugins/date.ts";
import esbuild from "lume/plugins/esbuild.ts";
import lightningcss from "lume/plugins/lightningcss.ts";
import metas from "lume/plugins/metas.ts";
import minify_html from "lume/plugins/minify_html.ts";
import multilanguage from "lume/plugins/multilanguage.ts";
import nav from "lume/plugins/nav.ts";
import pagefind from "lume/plugins/pagefind.ts";
import sheets from "lume/plugins/sheets.ts";
import sitemap from "lume/plugins/sitemap.ts";
import slugify_urls from "lume/plugins/slugify_urls.ts";

const site = lume({
	src: "./src",
	dest: "./public"
});

site.copy([".svg", ".jpg", ".webp", ".avif"]);
//site.ignore([""]);

site.use(attributes());
site.use(code_highlight());
site.use(date());
site.use(esbuild());
site.use(lightningcss());
site.use(metas());
site.use(minify_html());
site.use(multilanguage({
	languages: ["en", "fr", "de", "ja", "ru", "zh-TW", "zh-HK", "zh-CN"],
	defaultLanguage: "en"
}));
site.use(nav());
site.use(pagefind());
site.use(sheets({
	sheets: "first"
}));
site.use(sitemap());
site.use(slugify_urls());

export default site;
